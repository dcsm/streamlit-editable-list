from pathlib import Path

import pytest

from playwright.sync_api import expect, FrameLocator, Page

from e2e_utils import StreamlitRunner

SCRIPT_DIRECTORY = Path(__file__).parent.absolute()
APP_FILE = SCRIPT_DIRECTORY / "app.py"


@pytest.fixture(autouse=True, scope="module")
def streamlit_app():
    with StreamlitRunner(APP_FILE) as runner:
        yield runner


@pytest.fixture(autouse=True, scope="function")
def go_to_app(page: Page, streamlit_app: StreamlitRunner):
    page.goto(streamlit_app.server_url)
    # Wait for app to load
    print("Waiting for app to load...")
    page.get_by_role("img", name="Running...").is_hidden()
    print("App loaded!")


def get_editable_list_component(page: Page, index: int) -> FrameLocator:
    return page.frame_locator(
        'iframe[title="streamlit_editable_list.editable_list"]'
    ).nth(index)


def test_render_component(page: Page):
    print("Testing component...")
    print(page.url)
    component = get_editable_list_component(page, 0)
    result = page.get_by_role("table").nth(0)

    # Add new list entry
    component.get_by_role("button", name="add").nth(0).click()
    # Delete second list entry
    component.locator(".delete-entry").get_by_role("button").nth(0).click()
    # Write text in the new entry
    component.get_by_role("textbox").nth(1).fill(
        "Everyone"
    )  # Second input of type="text"
    # Save
    component.get_by_role("button", name="save").click()
    # Check resulting table (first column of new entry)
    cell = result.get_by_role("cell").nth(3)
    expect(cell).to_contain_text("Everyone")

    # Check auto-save component
    component = get_editable_list_component(page, 1)
    result = page.get_by_role("table").nth(1)
    # Write text in the first (and only) text input
    component.get_by_role("textbox").nth(0).fill("Hey")
    component.get_by_role("textbox").nth(0).blur()
    # Check resulting table
    cell = result.get_by_role("cell").nth(0)  # First cell has index 1!
    expect(cell).to_contain_text("Hey")
