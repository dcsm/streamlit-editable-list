import streamlit as st
from streamlit_editable_list import editable_list

st.header("Editable list")

input_params = [
    {
        "type": "text",
        "placeholder": "Enter text",
        "value": "",
    },
    {
        "type": "number",
        "placeholder": "Enter number",
        "value": 0,
    },
    {
        "list": "countries",
        "placeholder": "Select country",
        "value": "",
        "options": ["Switzerland", "Sweden", "France", "Germany"],
    }
]

initial_data = [
    ["Hello", 1, "Switzerland"],
    ["World", 2, "France"],
]

st.subheader("Component")
new_data = editable_list(initial_data, input_params)

st.subheader("New data")
st.table(new_data)


st.header("Editable list with auto-save")

input_params_auto_save = [
    {
        "type": "text",
        "placeholder": "Enter text",
        "value": "",
    },
]

initial_data_auto_save = [[""]]

new_data_auto_save = editable_list(
    initial_data_auto_save,
    input_params_auto_save,
    auto_save=True,
)

st.subheader("New data")
st.table(new_data_auto_save)
