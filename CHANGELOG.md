# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [v0.0.6](https://gitlab.com/dcsm/streamlit-editable-list/tags/v0.0.6) - 2025-03-10

- upgrade python versions (including in gitlab CI verifications)

<small>[Compare with latest](https://gitlab.com/dcsm/streamlit-editable-list/compare/v0.0.5...v0.0.6)</small>

### Fixed

- Fix Python 3.9 compatibility (type hints)
- Reduce list delete button width

## [v0.0.5](https://gitlab.com/dcsm/streamlit-editable-list/tags/v0.0.5) - 2023-12-19

<small>[Compare with latest](https://gitlab.com/dcsm/streamlit-editable-list/compare/v0.0.4...v0.0.5)</small>

### Fixed

- Fix Python 3.9 compatibility (type hints)
- Reduce list delete button width

## [v0.0.4](https://gitlab.com/dcsm/streamlit-editable-list/tags/v0.0.4) - 2023-12-05

<small>[Compare with latest](https://gitlab.com/dcsm/streamlit-editable-list/compare/v0.0.3...v0.0.4)</small>

### Added

- Add save status text
- Add auto-save

## [v0.0.3](https://gitlab.com/dcsm/streamlit-editable-list/tags/v0.0.3) - 2023-11-27

<small>[Compare with latest](https://gitlab.com/dcsm/streamlit-editable-list/compare/v0.0.2...v0.0.3)</small>

### Fixed

- Properly include frontend built code in wheels.

## [v0.0.2](https://gitlab.com/dcsm/streamlit-editable-list/tags/v0.0.2) - 2023-11-24

<small>[Compare with latest](https://gitlab.com/dcsm/streamlit-editable-list/compare/v0.0.1...v0.0.2)</small>

### Fixed

- Usage instructions in README.

## [v0.0.1](https://gitlab.com/dcsm/streamlit-editable-list/tags/v0.0.1) - 2023-11-24

<small>[Compare with first commit](https://gitlab.com/dcsm/streamlit-editable-list/compare/690a54e3263d23cf5e894cc19042ac64eb240d0b...v0.0.1)</small>

### Added

- Copy Streamlit component template (React)
- Add editable list with inputs
- Add python build and publish in CI
