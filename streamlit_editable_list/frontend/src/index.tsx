import React from "react"
import ReactDOM from "react-dom"
import EditableList from "./EditableList"

ReactDOM.render(
	<React.StrictMode>
		<EditableList />
	</React.StrictMode>,
	document.getElementById("root")
)
