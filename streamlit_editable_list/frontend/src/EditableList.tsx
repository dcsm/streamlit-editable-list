import {
	Streamlit,
	StreamlitComponentBase,
	withStreamlitConnection,
} from "streamlit-component-lib";
import React, { ReactNode } from "react";

import {
	getListEntryJsx,
	ListEntry,
} from "./ListEntry";

import {
	getDatalistOptionJsx,
	InputDescription,
} from "./Input";


interface State {
	data: ListEntry[],
	saved: boolean,
};


/**
 * This is a React-based component template. The `render()` function is called
 * automatically when the component should be re-rendered.
 */
export class EditableList extends StreamlitComponentBase<State> {
	private autoSave = this.props.args["auto_save"];
	public state = {
		// Arguments that are passed to the plugin in Python are accessible
		// via `this.props.args`.
		data: this.props.args["data"],
		saved: true,
	};

	public inputParameters: InputDescription[] = this.props.args["input_params"];

	public render = (): ReactNode => {
		// Streamlit sends us a theme object via props that we can use to ensure
		// that our component has visuals that match the active theme in a
		// streamlit app.
		const theme = this.props.theme;
		// const style: React.CSSProperties = {};
		const style: { [key: string]: string } = {};

		// Maintain compatibility with older versions of Streamlit that don't send
		// a theme object.
		if (theme) {
			for (const [key, value] of Object.entries(theme)) {
				style[`--${key}`] = value;
			}
			// console.log(style);
		}

		const listEntries = this.state.data.map(getListEntryJsx(this));
		const datalistOptions = this.inputParameters.map(getDatalistOptionJsx);
		const addButton = <button name="add" onClick={this.add}>Add entry</button>
		const saveButton = <button name="save" onClick={this.save}>Save</button>

		let saveStateClasses = ["save-state", this.state.saved ? "saved" : "unsaved"]
		let saveStateText = this.state.saved ? "Saved!" : "Not saved"
		const saveState = <p className={saveStateClasses.join(" ")}>{saveStateText}</p>

		return (
			<div style={style}>
				<table className="list-container">
					<tbody>{listEntries}</tbody>
				</table>
				{datalistOptions}
				<div className="list-buttons">
					{addButton}
					{this.autoSave ? "" : saveButton}
					{saveState}
				</div>
			</div>
		);
	};

	/** Click handler for our "Add entry" button. */
	private add = (): void => { // Use arrow function to not need to bind `this` in onClick
		this.setState(prevState => {
			const newData = [...prevState.data];
			const newListEntry = this.inputParameters.map((param: InputDescription) => param.value);
			newData.push(newListEntry);
			return { data: newData };
		});
		this.markUnsaved();
	}

	public markUnsaved = (): void => {
		if (this.autoSave) {
			this.save();
		} else {
			this.setState({ saved: false });
		}
	}

	/** Click handler for our "Save" button. */
	private save = (): void => {
		this.setState(
			prevState => ({ data: prevState.data, saved: true }),
			() => Streamlit.setComponentValue(this.state.data.filter((listEntry: Array<any> | null) => listEntry !== null))
		);
	};
}

export default withStreamlitConnection(EditableList);
