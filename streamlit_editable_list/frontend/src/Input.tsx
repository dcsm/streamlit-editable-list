import React from "react";
import { EditableList } from "./EditableList";


export interface InputDescription {
	list?: string,
	options?: string[],
	placeholder?: string,
	type?: string,
	value?: any,
};


type UpdateStateOnInputChange = (_: number) => (_: React.ChangeEvent<HTMLInputElement>) => void;

export const getInputJsx = (editableList: EditableList, updateState: UpdateStateOnInputChange) => (input: any, inputIndex: number): JSX.Element => {
	const inputAttributes = {...editableList.inputParameters[inputIndex]};
	delete inputAttributes.options; // "options" is not a valid attribute for input elements

	return <td key={inputIndex}>
		<input
			{...inputAttributes} // type, placeholder, ...
			onChange={updateState(inputIndex)}
			onBlur={editableList.markUnsaved}
			value={input}
		/>
	</td>
};


export function getDatalistOptionJsx(param: InputDescription): JSX.Element {
	if (param.options === undefined || param.list === undefined) {
		return <></>;
	}

	const options = param.options.map(
		(option: string, index: number) => <option key={index} value={option}></option>
	);
	return <datalist id={param.list}>{options}</datalist>;
}
