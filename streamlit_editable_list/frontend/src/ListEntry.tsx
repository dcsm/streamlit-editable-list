import React from "react";
import { EditableList } from "./EditableList";
import {
	getInputJsx,
} from "./Input";


export type ListEntry = any[] | null;


export const getListEntryJsx = (editableList: EditableList) => (listEntry: ListEntry, entryIndex: number): JSX.Element => {
	if (listEntry === null) {
		return <tr key={entryIndex} className="hidden"></tr>;
	}

	const updateState = (inputIndex: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
		const newValue = event.target.value;

		editableList.setState(prevState => {
			const newData = [...prevState.data];
			const newListEntry = [...listEntry];
			newData[entryIndex] = newListEntry;
			newListEntry[inputIndex] = newValue;

			return { data: newData };
		});
	};

	const inputs = listEntry.map(getInputJsx(editableList, updateState));

	const remove = () => {
		editableList.setState(prevState => {
			const newData = [...prevState.data];
			newData[entryIndex] = null; // Keep removed entry in list to maintain index
			return { data: newData };
		});
		editableList.markUnsaved();
	};

	const removeButton = <td className="delete-entry" key={listEntry.length}><button onClick={remove}>🗑</button></td>

	return <tr key={entryIndex}>
		{inputs}
		{removeButton}
	</tr>
};
