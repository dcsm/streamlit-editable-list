install:
	cd streamlit_editable_list/frontend && npm install
	pip install -e .
	pip install pytest playwright pytest-playwright
	playwright install chromium

start:
	cd streamlit_editable_list/frontend && npm start&
	CUSTOM_COMPONENT_DEVELOP=1 streamlit run examples/example.py

build:
	cd streamlit_editable_list/frontend && npm run build
	python -m build
